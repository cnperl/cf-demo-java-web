package web.servlet;

import org.apache.commons.io.FileUtils;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * User: Ulric Qin
 * Mail: qinxiaohui@xiaomi.com
 */
public class HelloServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String p = req.getParameter("p");
        if ("set".equals(p)) {
            // set cookie
            Cookie cookie1 = new Cookie("name", "abc");
            Cookie cookie2 = new Cookie("age", "11");
            resp.addCookie(cookie1);
            resp.addCookie(cookie2);
            resp.getWriter().print("Done");
        } else if ("get".equals(p)) {
            // get cookie
            Cookie[] cookies = req.getCookies();
            for (Cookie c : cookies) {
                resp.getWriter().printf("cookie name:%s, value:%s\n", c.getName(), c.getValue());
            }
        } else {
            resp.getWriter().print("Error");
        }
    }
}
